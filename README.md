Ausgangssituation
-----------------------

Die CarRent GmbH ist ein regional tätiger Anbieter von Mietfahrzeugen
aus Münster.

1 Angaben zum Unternehmen:
------------------------------
                                                      
  -   Firmensitz in Münster (Westfalen)               
                                                      
  -   Gründung 2009                                   
                                                      
  -   Geschäftsführer: Werner Kohler                  
                                                      
  -   Fahrzeugbestand: ca. 100                        
                                                      
  -   Durchschnittliches Fahrzeug-Alter: 3-4 Monate   
                                                      
  -   Mitarbeiter: 10                                 

Das Angebot an Mietfahrzeugen erstreckt sich auf PKW und Transporter. Bei den PKW stehen Fahrzeuge des Typs Kleinwagen, Mittelklasse und Oberklasse zur Verfügung. Für sie gelten pauschale Tagespreise ohne km-Begrenzung.

Bei den LKW stehen Fahrzeuge aus drei unterschiedlichen Nutzlastklassen zur Verfügung. Für jede Nutzlastklasse wird ein spezieller Preis angeboten, der sich aus einem Grundtarif pro Tag, einem km-Preis sowie einer speziellen Anzahl an Freikilometern zusammensetzt.
Für jedes vermietete Fahrzeug wird ein
eigener Mietvertrag abgeschlossen. Mietverträge über mehrere Fahrzeuge
werden nicht abgeschlossen. Die Fahrzeuge werden von den Kunden
grundsätzlich voll betankt am Firmensitz in Münster abgeholt und dort
auch voll betankt zurückgegeben.
Die CarRent GmbH verwaltet die für die
Vermietung erforderlichen Daten bislang über unterschiedliche
Büro-Anwendungen sowie in papiergebundener Form. Immer häufiger klagen
Mitarbeiter und Kunden, dass das Zusammenstellen der benötigten
Informationen sowie die Verarbeitung kurzfristiger Änderungen sehr
zeitaufwändig und fehleranfällig sind.

In der letzten Mitarbeitersitzung wurde daher beschlossen, ein
IT-Unternehmen damit zu beauftragen, ein einheitliches
Informationssystem für die CarRent GmbH zu entwickeln.

2 Projektplanung
----------------

Herr Kohler, der Geschäftsführer der CarRent GmbH, setzt sich auf
Empfehlung von Geschäftspartnern mit dem Software-Unternehmen InSoft in
Verbindung.

Die InSoft GmbH ist ein junges, innovatives Unternehmen aus
Gelsenkirchen, das sich auf die Erstellung individueller
Softwarelösungen für Unternehmen spezialisiert hat. Projekte werden
durch ein interdisziplinäres Team bearbeitet, in dem jedes Teammitglied
auf ein bestimmtes Fachgebiet spezialisiert ist.

Nach mehreren Gesprächsrunden erteilt die CarRent GmbH an die InSoft
GmbH den Auftrag zur Erstellung einer IT-Lösung für die Abwicklung des
Vermietungsgeschäftes. CarRent hält die genauen Wünsche in einem
Lastenheft fest. Die Softwareanwendung soll individuell auf die
Kernfunktionen der Vermietungsprozesse der CarRent GmbH zugeschnitten
sein und sich mittelfristig so erweitern lassen, dass alle
Geschäftsprozesse maßgeschneidert mit dem neuen System realisiert werden
können. In beiden Unternehmen werden im Vorfeld des Projektes
Mitarbeiter bestimmt, die für die Projektdurchführung verantwortlich
sind.

Für die CarRent GmbH sind der Geschäftsführer, Herr Kohler, und die
Kundenbetreuerin Beatrix Glasmer die Ansprechpartner für dieses Projekt.

3 Projektdurchführung
------------------------

Bei der Projektdurchführung orientieren sich die Projektgruppen an den
klassischen Projektphasen des Software-Engineering. Zu Beginn des
Projektes kommen alle Beteiligten im Haus der CarRent GmbH zusammen.

Formular 1:

  **CarRent GmbH**
  
  **Aktuelle Preisliste** (Stand: 01.Juli 2018)

**Mietfahrzeuge PKW**

|Typ|Technische Daten|Schutz|Preis pro Tag|
|---|----------------|------|-------------|
|P1 – PKW Kleinwagen|4-Türer Klimaanlage freie km|Vollkasko ohne Selbst-beteiligung|55€|
|P2 – PKW Mittelklasse|4-TürerKlimaanlagefreie km|Vollkasko ohne Selbst-beteiligung|55€|
|PO- PKW Oberklasse|4-TürerKlimaanlagefreie km|Vollkasko ohne Selbst-beteiligung|125€|


  **Mietfahrzeuge LKW**


|Typ|Technische Daten|Schutz|Preis pro Tag|
|---|----------------|------|-------------|
|L1 –LKW Nutzlast bis 1000 kg|Laderaum: 11.2 m³ Nutzlast 985 kg|Vollkasko ohne Selbst-beteiligung|Grundtarif: 95,00 €; km-Preis: 0,24 €; freie km: 100|
|L2 – LKW Nutzlast bis 1600 kg|Laderaum: 14,8 m³ Nutzlast 1220 kg|Vollkasko ohne Selbst-beteiligung|Grundtarif: 135,00 €; km-Preis: 0,36 €; freie km: 200|
|L3 – LKW Nutzlast bis 2500 kg|Laderaum:35,2 m³ Nutzlast 2450 kg|Vollkasko ohne Selbst-beteiligung|Grundtarif: 300,00 €; km-Preis: 0,61 €; freie km: 300|

**Aufgabe 1:**

Erstellen Sie ein neues Projekt zur Verwaltung von Mietvorgängen bei der CarRent in Anlehnung an das Kontoprojekt.

**Aufgabe 2:**

Erweitern Sie das Projekt zur Verwaltung von Mietvorgängen in Anlehnung an das Kontoprojekt. Beim Laden des
Formulars sollen die Objekte P1, P2, PO deklariert, instanziiert und initialisiert werden. Ein Objekt namens MeinPKW wird
deklariert. Beim Klick auf PKW anlegen soll MeinPKW instanziiert und initialisiert werden. 

**Aufgabe 3:**

Erweitern Sie das Projekt zur Vermietung von PKWs in Anlehnung an das Kontoprojekt, indem Sie den
Vermietungsvorgang für MeinPKW (siehe die obigen Vermietungsbeispiele)
implementieren. Die Rechungsbelege (siehe oben) zeigen, welche Daten auf
dem Label angezeigt werden sollen.

Am Ende soll auf Knopfdruck auf dem Label die Rechnung mit den
relevanten Rechnungsdaten abgebildet werden.
